################################################################################
#
# foo_package
#
################################################################################

FOO_VERSION = 1.0
FOO_SITE = $(BR2_EXTERNAL_PACKAGE_PATH)/package/foo_package/src
FOO_SITE_METHOD = local

define FOO_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D)
endef

define FOO_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/foo_package $(TARGET_DIR)/usr/bin
endef

$(eval $(generic-package))
